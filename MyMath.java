package bcas.ap.test.junit;

public class MyMath {
	public int addnum(int num1 , int num2) {
		return num1 + num2;
	}
	public int subnum(int num1 , int num2) {
		return num1 - num2;
	}
	public int mulnum(int num1 , int num2) {
		return num1 * num2;
	}
	public int dinum(int num1 , int num2) {
		return num1 / num2;
	}

}

package bcas.ap.test.junit;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MyMathDemo {
	MyMath mymath  = null;
	@Before
	public void setup() {
		mymath = new MyMath();
		
	}
	@Test
	public void addnumPosstive() {
		int result = mymath.addnum(20, 70);
		int result1 = mymath.addnum(30, 80);
		int result2 = mymath.addnum(40, 90);
		int result3 = mymath.addnum(50, 100);
		
		
		Assert.assertEquals(90,result);
		Assert.assertEquals(110,result);
		Assert.assertEquals(130,result);
		Assert.assertEquals(150,result);
	}
	

}
